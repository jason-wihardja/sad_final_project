/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author
 * Jason
 */
public class Event {
    private Integer eventID;
    private String name;
    private String location;
    
    public Event(Integer eventID, String name, String location) {
        this.eventID = eventID;
        this.name = name;
        this.location = location;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
