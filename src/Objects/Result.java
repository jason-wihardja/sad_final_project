/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author
 * Jason
 */
public class Result {
    private Integer partID;
    private String fName;
    private String lName;
    private String category;
    private Integer teamID;
    private String origin;
    private Integer resultID;
    private Integer eventID;
    private Integer startNo;
    private String kelas;
    private String car;
    private Integer time1;
    private Integer time2;
    private Integer time3;
    private Integer penalty1;
    private Integer penalty2;
    private Integer penalty3;

    public Result() {
        partID = -1;
        fName = "";
        lName = "";
        category = "";
        teamID = -1;
        origin = "";
        resultID = -1;
        eventID = -1;
        startNo = -1;
        kelas = "";
        car = "";
        time1 = 0;
        time2 = 0;
        time3 = 0;
        penalty1 = 0;
        penalty2 = 0;
        penalty3 = 0;
    }

    public Integer getPartID() {
        return partID;
    }

    public void setPartID(Integer partID) {
        this.partID = partID;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getTeamID() {
        return teamID;
    }

    public void setTeamID(Integer teamID) {
        this.teamID = teamID;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Integer getResultID() {
        return resultID;
    }

    public void setResultID(Integer resultID) {
        this.resultID = resultID;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getStartNo() {
        return startNo;
    }

    public void setStartNo(Integer startNo) {
        this.startNo = startNo;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public Integer getTime1() {
        return time1;
    }

    public void setTime1(Integer time1) {
        this.time1 = time1;
    }

    public Integer getTime2() {
        return time2;
    }

    public void setTime2(Integer time2) {
        this.time2 = time2;
    }

    public Integer getTime3() {
        return time3;
    }

    public void setTime3(Integer time3) {
        this.time3 = time3;
    }

    public Integer getPenalty1() {
        return penalty1;
    }

    public void setPenalty1(Integer penalty1) {
        this.penalty1 = penalty1;
    }

    public Integer getPenalty2() {
        return penalty2;
    }

    public void setPenalty2(Integer penalty2) {
        this.penalty2 = penalty2;
    }

    public Integer getPenalty3() {
        return penalty3;
    }

    public void setPenalty3(Integer penalty3) {
        this.penalty3 = penalty3;
    }
}
