/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 * @author
 * Jason
 * Wihardja
 */
public class TeamResult implements Comparable<TeamResult> {

    private Team teamInfo;
    private Integer time;
    private Integer penalty;

    public TeamResult(Integer teamID, String teamName) {
        this.teamInfo = new Team(teamID, teamName);
        this.time = 0;
        this.penalty = 0;
    }

    public Team getTeamInfo() {
        return this.teamInfo;
    }

    public void addTime(Integer ms) {
        this.time += ms;
    }

    public Integer getTime() {
        return this.time;
    }

    public void addPenalty(Integer ms) {
        this.penalty += ms;
    }

    public Integer getPenalty() {
        return this.penalty;
    }

    @Override
    public int compareTo(TeamResult o) {
        Integer thisTotal = time + penalty;
        Integer thatTotal = o.getTime() + o.getPenalty();

        if (thisTotal == thatTotal) {
            return teamInfo.getName().compareTo(o.getTeamInfo().getName());
        } else {
            return thisTotal - thatTotal;
        }
    }
}
