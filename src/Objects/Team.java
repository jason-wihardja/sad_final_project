package Objects;

/**
 *
 * @author Kevin
 */
public class Team {
    private int teamID;
    private String name;
    
    public Team() {
        
    }
    
    public Team (int teamID, String name) {
        this.teamID = teamID;
        this.name = name;
    }
    
    //Getters
    public int getTeamID() {
        return teamID;
    }

    public String getName() {
        return name;
    }
    
    //Setters
    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
