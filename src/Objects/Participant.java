package Objects;

/**
 * Participant bean
 * @author Kevin
 */
public class Participant {
    private int partID;
    private String fName;
    private String lName;
    private String category;
    private int teamID;
    private String origin;
    
    /**
     * Blank constructor
     */
    public Participant() {
        
    }
    
    /**
     * Parameterized constructor.
     * @param partID
     * @param fName
     * @param lName
     * @param category
     * @param teamID
     * @param origin 
     */
    public Participant(int partID, String fName, String lName, String category, int teamID, String origin) {
        this.partID = partID;
        this.fName = fName;
        this.lName = lName;
        this.category = category;
        this.teamID = teamID;
        this.origin = origin;
    }
    
    //Getters
    public int getPartID() {
        return partID;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getCategory() {
        return category;
    }

    public int getTeamID() {
        return teamID;
    }

    public String getOrigin() {
        return origin;
    }
    
    //Setters
    public void setPartID(int partID) {
        this.partID = partID;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
