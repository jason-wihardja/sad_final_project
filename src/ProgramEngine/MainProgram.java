package ProgramEngine;

import Objects.Event;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/**
 * @author
 * Jason
 * Wihardja
 */
public class MainProgram extends JFrame {

    private String mysqlUsername = "root";
    private String mysqlPassword = "admin";
    private String eventName = "No Event Selected";
    private DefaultTableModel myTableModel;
    private Integer eventID = -1;

    /**
     * Creates
     * new
     * form
     * MainProgram
     */
    public MainProgram() {
        setWindowPosition();
        initTableModel();
        initComponents();
        initTableColumnWidth();
        syncTable();
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * set
     * the
     * position
     * of
     * the
     * window
     * to
     * the
     * center
     * of
     * the
     * screen.
     */
    private void setWindowPosition() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((dim.width - 1366) / 2, (dim.height - 600) / 3);
    }

    private String getTimeFromMillisecond(Integer time, Integer penalty) {
        String result = "";

        result += String.format("%02d:%02d,%03d",
                TimeUnit.MILLISECONDS.toMinutes(time),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)),
                time % 1000);

        result += " ";

        result += String.format("(%02d:%02d,%03d)",
                TimeUnit.MILLISECONDS.toMinutes(penalty),
                TimeUnit.MILLISECONDS.toSeconds(penalty) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(penalty)),
                penalty % 1000);

        return result;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
        
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
            Statement statement = connection.createStatement();
            
            ResultSet resultSet = statement.executeQuery("SELECT * FROM event WHERE eventID = " + eventID + ";");
            while (resultSet.next()) {
                this.eventName = resultSet.getString("name");
            }
            
            syncTable();
        } catch (SQLException e) {
            //Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, e);
            System.out.println(e.toString());
        }
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * table
     * model.
     */
    public final void initTableModel() {
        String columns[] = {"Pos.", "No.", "Nama Peserta", "Tim", "Kendaraan", "Asal", "Kategori", "Kelas", "Heat 1", "Heat 2", "Heat 3", "Total Waktu"};
        this.myTableModel = new DefaultTableModel(null, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * column
     * width
     * in
     * the
     * table.
     */
    public final void initTableColumnWidth() {
        for (int i = 0; i < 12; i++) {
            int widths[] = {35, 35, 200, 200, 200, 125, 70, 40, 160, 160, 160, 160};
            resultTable.getColumnModel().getColumn(i).setPreferredWidth(widths[i]);
        }
    }

    /**
     * This
     * method
     * is
     * called
     * whenever
     * an
     * update
     * to
     * the
     * table
     * content
     * is
     * needed.
     */
    public final void syncTable() {
        setTitle("Slalom Race Management System [" + eventName + "]");
        myTableModel.setRowCount(0);

        if (eventID >= 0) {
            try {
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
                Statement statement = connection.createStatement();

                String[] resultData = new String[12];
                Integer position = 1;

                String sql = "SELECT * FROM participant NATURAL JOIN team NATURAL JOIN results WHERE eventID = " + eventID;
                if (!classComboBox.getSelectedItem().toString().equals("All class") && !categoryComboBox.getSelectedItem().toString().equals("All category")) {
                    // User selects specific class & specific category
                    String kelas = classComboBox.getSelectedItem().toString().substring(6);
                    String kategori = categoryComboBox.getSelectedItem().toString();
                    sql += " AND class = '" + kelas + "' AND category = '" + kategori + "'";
                } else {
                    if (!classComboBox.getSelectedItem().toString().equals("All class")) {
                        // User selects specific class
                        String kelas = classComboBox.getSelectedItem().toString().substring(6);
                        sql += " AND class = '" + kelas + "'";
                    }

                    if (!categoryComboBox.getSelectedItem().toString().equals("All category")) {
                        // User selects specific category
                        String kategori = categoryComboBox.getSelectedItem().toString();
                        System.out.println(kategori);
                        sql += " AND category = '" + kategori + "'";
                    }
                }
                sql += " ORDER BY (time1 + time2 + time3 + penalty1 + penalty2 + penalty3) ASC;";

                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    if (!resultSet.getString("time3").equals("0") && !resultSet.getString("penalty3").equals("0")) {
                        resultData[0] = String.format("%d", position++);
                        resultData[1] = resultSet.getString("startNo");
                        resultData[2] = resultSet.getString("fName") + " " + resultSet.getString("lName");
                        resultData[3] = resultSet.getString("name");
                        resultData[4] = resultSet.getString("car");
                        resultData[5] = resultSet.getString("origin");
                        resultData[6] = resultSet.getString("category");
                        resultData[7] = resultSet.getString("class");

                        Integer time1 = Integer.parseInt(resultSet.getString("time1"));
                        Integer time2 = Integer.parseInt(resultSet.getString("time2"));
                        Integer time3 = Integer.parseInt(resultSet.getString("time3"));
                        Integer totalTime = time1 + time2 + time3;
                        Integer penalty1 = Integer.parseInt(resultSet.getString("penalty1"));
                        Integer penalty2 = Integer.parseInt(resultSet.getString("penalty2"));
                        Integer penalty3 = Integer.parseInt(resultSet.getString("penalty3"));
                        Integer totalPenalty = penalty1 + penalty2 + penalty3;

                        resultData[8] = getTimeFromMillisecond(time1, penalty1);
                        resultData[9] = getTimeFromMillisecond(time2, penalty2);
                        resultData[10] = getTimeFromMillisecond(time3, penalty3);
                        resultData[11] = getTimeFromMillisecond(totalTime, totalPenalty);

                        this.myTableModel.addRow(resultData.clone());
                    }
                }

                resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    if (resultSet.getString("time3").equals("0") && resultSet.getString("penalty3").equals("0")) {
                        resultData[0] = String.format("%d", position++);
                        resultData[1] = resultSet.getString("startNo");
                        resultData[2] = resultSet.getString("fName") + " " + resultSet.getString("lName");
                        resultData[3] = resultSet.getString("name");
                        resultData[4] = resultSet.getString("car");
                        resultData[5] = resultSet.getString("origin");
                        resultData[6] = resultSet.getString("category");
                        resultData[7] = resultSet.getString("class");

                        Integer time1 = Integer.parseInt(resultSet.getString("time1"));
                        Integer time2 = Integer.parseInt(resultSet.getString("time2"));
                        Integer time3 = Integer.parseInt(resultSet.getString("time3"));
                        Integer totalTime = time1 + time2 + time3;
                        Integer penalty1 = Integer.parseInt(resultSet.getString("penalty1"));
                        Integer penalty2 = Integer.parseInt(resultSet.getString("penalty2"));
                        Integer penalty3 = Integer.parseInt(resultSet.getString("penalty3"));
                        Integer totalPenalty = penalty1 + penalty2 + penalty3;

                        resultData[8] = getTimeFromMillisecond(time1, penalty1);
                        resultData[9] = getTimeFromMillisecond(time2, penalty2);
                        resultData[10] = getTimeFromMillisecond(time3, penalty3);
                        resultData[11] = getTimeFromMillisecond(totalTime, totalPenalty);

                        this.myTableModel.addRow(resultData.clone());
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * form.
     * WARNING:
     * Do
     * NOT
     * modify
     * this
     * code.
     * The
     * content
     * of
     * this
     * method
     * is
     * always
     * regenerated
     * by
     * the
     * Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        optionsPanel = new javax.swing.JPanel();
        classLabel = new javax.swing.JLabel();
        classComboBox = new javax.swing.JComboBox();
        categoryLabel = new javax.swing.JLabel();
        categoryComboBox = new javax.swing.JComboBox();
        resultPanel = new javax.swing.JPanel();
        scrollPane = new javax.swing.JScrollPane();
        resultTable = new javax.swing.JTable();
        timeInputBtnPanel = new javax.swing.JPanel();
        editTimeBtn = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        selectEventMenu = new javax.swing.JMenu();
        teamResultMenu = new javax.swing.JMenu();
        manageMenu = new javax.swing.JMenu();
        eventsMenuItem = new javax.swing.JMenuItem();
        teamsMenuItem = new javax.swing.JMenuItem();
        racersMenuItem = new javax.swing.JMenuItem();
        aboutMenu = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Slalom Race Management System [No Event Selected]");
        setMinimumSize(new java.awt.Dimension(1366, 600));
        setName("mainFrame"); // NOI18N
        setResizable(false);

        optionsPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(3, 1, 2, 1));
        optionsPanel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        optionsPanel.setLayout(new java.awt.GridBagLayout());

        classLabel.setText("Class");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 32);
        optionsPanel.add(classLabel, gridBagConstraints);

        classComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "All class", "Kelas A1", "Kelas A2", "Kelas A3", "Kelas A Saloon", "Kelas B", "Kelas C", "Kelas D", "Kelas F", "Kelas F - RWD" }));
        classComboBox.setPreferredSize(new java.awt.Dimension(1200, 21));
        classComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        optionsPanel.add(classComboBox, gridBagConstraints);

        categoryLabel.setText("Category");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 32);
        optionsPanel.add(categoryLabel, gridBagConstraints);

        categoryComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "All category", "Pemula", "Seeded A", "Seeded B" }));
        categoryComboBox.setPreferredSize(new java.awt.Dimension(1200, 21));
        categoryComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categoryComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        optionsPanel.add(categoryComboBox, gridBagConstraints);

        getContentPane().add(optionsPanel, java.awt.BorderLayout.PAGE_START);

        resultPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(2, 1, 1, 1));
        resultPanel.setLayout(new java.awt.GridLayout(1, 1));

        resultTable.setModel(this.myTableModel);
        resultTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        resultTable.getTableHeader().setResizingAllowed(false);
        resultTable.getTableHeader().setReorderingAllowed(false);
        scrollPane.setViewportView(resultTable);

        resultPanel.add(scrollPane);

        getContentPane().add(resultPanel, java.awt.BorderLayout.CENTER);

        timeInputBtnPanel.setLayout(new java.awt.GridLayout(1, 1));

        editTimeBtn.setText("Edit Time");
        editTimeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editTimeBtnMouseClicked(evt);
            }
        });
        timeInputBtnPanel.add(editTimeBtn);

        getContentPane().add(timeInputBtnPanel, java.awt.BorderLayout.PAGE_END);

        selectEventMenu.setText("Select Event");
        selectEventMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                selectEventMenuMouseClicked(evt);
            }
        });
        menuBar.add(selectEventMenu);

        teamResultMenu.setText("Team Result");
        teamResultMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                teamResultMenuMouseClicked(evt);
            }
        });
        menuBar.add(teamResultMenu);

        manageMenu.setText("Manage");

        eventsMenuItem.setText("Events");
        eventsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eventsMenuItemActionPerformed(evt);
            }
        });
        manageMenu.add(eventsMenuItem);

        teamsMenuItem.setText("Teams");
        teamsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                teamsMenuItemActionPerformed(evt);
            }
        });
        manageMenu.add(teamsMenuItem);

        racersMenuItem.setText("Racers");
        racersMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                racersMenuItemActionPerformed(evt);
            }
        });
        manageMenu.add(racersMenuItem);

        menuBar.add(manageMenu);

        aboutMenu.setText("About");
        aboutMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aboutMenuMouseClicked(evt);
            }
        });
        menuBar.add(aboutMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void aboutMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_aboutMenuMouseClicked
        // System.out.println("About Menu Clicked");
        JOptionPane.showMessageDialog(this, "Slalom Race Management System v1.1\n\nCreated by:\n- Jason Wihardja\n- Immanuel Gregorius\n- Kevin Nursalim\n- William Christian Lie", "About", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_aboutMenuMouseClicked

    private void teamsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_teamsMenuItemActionPerformed
        JFrame teamManager = new JFrame("Team Manager");
        teamManager.setSize(800, 600);

        teamManager.add(new TeamManager());
        teamManager.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        teamManager.setResizable(false);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        teamManager.setLocation((dim.width - 800) / 2, (dim.height - 600) / 3);

        teamManager.pack();
        teamManager.setLocationRelativeTo(this);
        teamManager.setVisible(true);
    }//GEN-LAST:event_teamsMenuItemActionPerformed

    private void racersMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_racersMenuItemActionPerformed
        if (eventID >= 0) {
            JFrame racersManager = new JFrame("Racer Manager [" + eventName + "]");
            racersManager.setSize(800, 600);
            
            racersManager.add(new RacerManager(eventID, this));
            racersManager.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            racersManager.setResizable(false);
            
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            racersManager.setLocation((dim.width - 800) / 2, (dim.height - 600) / 3);
            
            racersManager.pack();
            racersManager.setLocationRelativeTo(this);
            racersManager.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "Please select an event!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_racersMenuItemActionPerformed

    private void eventsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eventsMenuItemActionPerformed
        JFrame eventManager = new JFrame("Event Manager");
        eventManager.setSize(640, 480);

        eventManager.add(new EventManager());
        eventManager.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        eventManager.setResizable(false);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        eventManager.setLocation((dim.width - 640) / 2, (dim.height - 480) / 3);

        eventManager.pack();
        eventManager.setLocationRelativeTo(this);
        eventManager.setVisible(true);
    }//GEN-LAST:event_eventsMenuItemActionPerformed

    private void selectEventMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_selectEventMenuMouseClicked
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
            Statement statement = connection.createStatement();

            ArrayList<Event> events = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM event;");
            while (resultSet.next()) {
                Integer id = Integer.parseInt(resultSet.getString(1));
                String name = resultSet.getString(2);
                String location = resultSet.getString(3);
                Event e = new Event(id, name, location);
                events.add(e);
            }

            String eventNames[] = new String[events.size()];
            for (int i = 0; i < eventNames.length; i++) {
                eventNames[i] = events.get(i).getName();
            }

            String selectedEvent = (String) JOptionPane.showInputDialog(this, "Event:", "Select Event", JOptionPane.QUESTION_MESSAGE, null, eventNames, eventNames[0]);
            if (selectedEvent != null && selectedEvent.length() != 0) {
                for (Event e : events) {
                    if (e.getName().equals(selectedEvent)) {
                        eventID = e.getEventID();
                        eventName = e.getName();
                        syncTable();
                        break;
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }//GEN-LAST:event_selectEventMenuMouseClicked

    private void categoryComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categoryComboBoxActionPerformed
        //System.out.println(this.categoryComboBox.getSelectedItem().toString());
        syncTable();
    }//GEN-LAST:event_categoryComboBoxActionPerformed

    private void classComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classComboBoxActionPerformed
        //System.out.println(this.classComboBox.getSelectedItem().toString());
        syncTable();
    }//GEN-LAST:event_classComboBoxActionPerformed

    private void editTimeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editTimeBtnMouseClicked
        //System.out.println( "No. Start = " + myTableModel.getValueAt(resultTable.getSelectedRow(), 1) );
        if (eventID >= 0) {
            try {
                JFrame timeEditor = new JFrame(myTableModel.getValueAt(resultTable.getSelectedRow(), 2).toString());
                timeEditor.setSize(577, 152);

                timeEditor.add(new TimeEditor(this, eventID, Integer.parseInt(myTableModel.getValueAt(resultTable.getSelectedRow(), 1).toString())));
                timeEditor.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                timeEditor.setResizable(false);

                Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                timeEditor.setLocation((dim.width - 577) / 2, (dim.height - 152) / 3);

                timeEditor.pack();
                timeEditor.setVisible(true);
            } catch (ArrayIndexOutOfBoundsException e) {
                //System.out.println(e.toString());
                JOptionPane.showMessageDialog(this, "Please select a person to edit!", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_editTimeBtnMouseClicked

    private void teamResultMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_teamResultMenuMouseClicked
        if (eventID >= 0) {
            JFrame teamResultViewer = new JFrame("Team Results [" + eventName + "]");
            teamResultViewer.setSize(600, 400);
            
            teamResultViewer.add(new TeamResultViewer(eventID));
            teamResultViewer.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            teamResultViewer.setResizable(false);
            
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            teamResultViewer.setLocation((dim.width - 600) / 2, (dim.height - 400) / 3);
            
            teamResultViewer.pack();
            teamResultViewer.setLocationRelativeTo(this);
            teamResultViewer.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "Please select an event!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_teamResultMenuMouseClicked

    /**
     * @param
     * args
     * the
     * command
     * line
     * arguments
     */
    public static void main(String args[]) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainProgram().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu aboutMenu;
    private javax.swing.JComboBox categoryComboBox;
    private javax.swing.JLabel categoryLabel;
    private javax.swing.JComboBox classComboBox;
    private javax.swing.JLabel classLabel;
    private javax.swing.JButton editTimeBtn;
    private javax.swing.JMenuItem eventsMenuItem;
    private javax.swing.JMenu manageMenu;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JPanel optionsPanel;
    private javax.swing.JMenuItem racersMenuItem;
    private javax.swing.JPanel resultPanel;
    private javax.swing.JTable resultTable;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JMenu selectEventMenu;
    private javax.swing.JMenu teamResultMenu;
    private javax.swing.JMenuItem teamsMenuItem;
    private javax.swing.JPanel timeInputBtnPanel;
    // End of variables declaration//GEN-END:variables
}
