package ProgramEngine;

import Objects.Team;
import dataaccess.DBUtil;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author
 * Kevin
 */
public class RacerAdder extends javax.swing.JPanel {

    protected JFrame container;
    protected ArrayList<Team> teamList;
    protected MainProgram mainProgram;

    /**
     * Creates
     * new
     * form
     * RacerAdder
     */
    public RacerAdder(MainProgram m) {
        this.mainProgram = m;
        container = null;
        teamList = new DBUtil().getTeams();

        initComponents();
        initComboBox();
    }

    public RacerAdder(MainProgram m, JFrame container) {
        this.mainProgram = m;
        this.container = container;
        teamList = new DBUtil().getTeams();

        initComponents();
        initComboBox();
    }

    /**
     * Initialize
     * the
     * combo
     * box
     */
    protected final void initComboBox() {
        if (this.teamList == null) {
            JOptionPane.showMessageDialog(null, "Team list is null!", "Error!", JOptionPane.ERROR_MESSAGE);
            if (this.container != null) {
                this.container.setVisible(false);
                this.container.dispose();
            }
        } else {
            String[] data = new String[teamList.size()];

            for (int i = 0; i < teamList.size(); i++) {
                data[i] = teamList.get(i).getName();
            }

            this.teamBox.setModel(new DefaultComboBoxModel(data));
        }
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * form.
     * WARNING:
     * Do
     * NOT
     * modify
     * this
     * code.
     * The
     * content
     * of
     * this
     * method
     * is
     * always
     * regenerated
     * by
     * the
     * Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        fNameLabel = new javax.swing.JLabel();
        lNameLabel = new javax.swing.JLabel();
        categoryLabel = new javax.swing.JLabel();
        teamLabel = new javax.swing.JLabel();
        originLabel = new javax.swing.JLabel();
        fNameField = new javax.swing.JTextField();
        lNameField = new javax.swing.JTextField();
        categoryField = new javax.swing.JTextField();
        teamBox = new javax.swing.JComboBox();
        originField = new javax.swing.JTextField();
        submitButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        titleLabel.setText("Add a Participant");

        fNameLabel.setText("First name");

        lNameLabel.setText("Last name");

        categoryLabel.setText("Category");

        teamLabel.setText("Team");

        originLabel.setText("Origin");

        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(titleLabel)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(fNameLabel)
                        .addGap(18, 18, 18)
                        .addComponent(fNameField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(originLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(teamLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(categoryLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(originField, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(lNameField)
                            .addComponent(categoryField)
                            .addComponent(teamBox, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(22, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(resetButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(submitButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fNameLabel)
                    .addComponent(fNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lNameLabel)
                    .addComponent(lNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(categoryLabel)
                    .addComponent(categoryField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(teamLabel)
                    .addComponent(teamBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(originLabel)
                    .addComponent(originField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(submitButton)
                    .addComponent(resetButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Reset
     * button
     * action
     * listener
     *
     * @param
     * evt
     */
    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        this.fNameField.setText("");
        this.lNameField.setText("");
        this.categoryField.setText("");
        this.teamBox.setSelectedIndex(0);
        this.originField.setText("");
    }//GEN-LAST:event_resetButtonActionPerformed

    /**
     * Submit
     * button
     * action
     * listener
     *
     * @param
     * evt
     */
    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        String fName = this.fNameField.getText();
        String lName = this.lNameField.getText();
        String category = this.categoryField.getText();
        //String team = this.teamList.get(this.teamBox.getSelectedIndex()).getName();
        int teamID = this.teamList.get(this.teamBox.getSelectedIndex()).getTeamID();
        String origin = this.originField.getText();

        if (fName.equals("")) {
            JOptionPane.showMessageDialog(container, "First name should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (lName.equals("")) {
            JOptionPane.showMessageDialog(container, "Last name should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (category.equals("")) {
            JOptionPane.showMessageDialog(container, "Category should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (origin.equals("")) {
            JOptionPane.showMessageDialog(container, "Origin should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            //JOptionPane.showMessageDialog(null, fName+"\n"+lName+"\n"+category+"\n"+teamID+"\n"+origin);
            if (new DBUtil().addParticipant(fName, lName, category, teamID, origin)) {
                JOptionPane.showMessageDialog(container, "Data input successful!", "Message", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(container, "Data input failed...", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        RacerManager.refresh();
        mainProgram.syncTable();
        
        if (this.container != null) {
            this.container.setVisible(false);
            this.container.dispose();
        }
    }//GEN-LAST:event_submitButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JTextField categoryField;
    protected javax.swing.JLabel categoryLabel;
    protected javax.swing.JTextField fNameField;
    protected javax.swing.JLabel fNameLabel;
    protected javax.swing.JTextField lNameField;
    protected javax.swing.JLabel lNameLabel;
    protected javax.swing.JTextField originField;
    protected javax.swing.JLabel originLabel;
    protected javax.swing.JButton resetButton;
    protected javax.swing.JButton submitButton;
    protected javax.swing.JComboBox teamBox;
    protected javax.swing.JLabel teamLabel;
    protected javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
