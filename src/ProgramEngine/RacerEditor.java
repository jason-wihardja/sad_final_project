package ProgramEngine;

import Objects.Participant;
import dataaccess.DBUtil;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Racer editor class
 * @author Kevin
 */
public class RacerEditor extends RacerAdder {
    private Participant p;
    
    public RacerEditor(MainProgram m, JFrame container, int partID) {
        super(m, container);
        p = new DBUtil().getParticipantByID(partID);
        
        this.titleLabel.setText("Edit a participant");
        this.fNameField.setText(p.getfName());
        this.lNameField.setText(p.getlName());
        this.categoryField.setText(p.getCategory());
        for (int i = 0; i < this.teamList.size(); i++) {
            if (this.teamList.get(i).getTeamID() == p.getTeamID()) {
                this.teamBox.setSelectedIndex(i);
                break;
            }
        }
        
        this.originField.setText(p.getOrigin());
        
        //remove all inherited action listener (has to be done because netbeans 
        //form automatically declared the function as private)
        for (java.awt.event.ActionListener l: submitButton.getActionListeners()) {
            submitButton.removeActionListener(l);
        }
        
        //add the new action listener
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });
    }
    
    /**
     * Submit button action listener
     * @param evt 
     */
    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        String fName = this.fNameField.getText();
        String lName = this.lNameField.getText();
        String category = this.categoryField.getText();
        //String team = this.teamList.get(this.teamBox.getSelectedIndex()).getName();
        int teamID = this.teamList.get(this.teamBox.getSelectedIndex()).getTeamID();
        String origin = this.originField.getText();
        
        if (fName.equals("")) {
            JOptionPane.showMessageDialog(container, "First name should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (lName.equals("")) {
            JOptionPane.showMessageDialog(container, "Last name should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (category.equals("")) {
            JOptionPane.showMessageDialog(container, "Category should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (origin.equals("")) {
            JOptionPane.showMessageDialog(container, "Origin should not be left empty", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            //JOptionPane.showMessageDialog(null, fName+"\n"+lName+"\n"+category+"\n"+teamID+"\n"+origin);
            if (new DBUtil().updateParticipant(this.p.getPartID(), fName, lName, category, teamID, origin)) {
                JOptionPane.showMessageDialog(container, "Data update successful!", "Message", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(container, "Data update failed...", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        RacerManager.refresh();
        mainProgram.syncTable();
        if (this.container != null) {
            this.container.setVisible(false);
            this.container.dispose();
        }
    }                          
}
