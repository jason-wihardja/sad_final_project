/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramEngine;

import Objects.TeamResult;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author
 * Jason
 */
public class TeamResultViewer extends javax.swing.JPanel {

    private Integer eventID;
    private DefaultTableModel myTableModel;
    private ArrayList<TeamResult> teamResults;
    private String mysqlUsername = "root";
    private String mysqlPassword = "";

    /**
     * Creates
     * new
     * form
     * TeamResultViewer
     */
    public TeamResultViewer(Integer eventID) {
        this.eventID = eventID;
        initTableModel();
        initComponents();
        initTableColumnWidth();
        syncTable();
    }

    private String getTimeFromMillisecond(Integer time, Integer penalty) {
        String result = "";

        result += String.format("%02d:%02d,%03d",
                TimeUnit.MILLISECONDS.toMinutes(time),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)),
                time % 1000);

        result += " ";

        result += String.format("(%02d:%02d,%03d)",
                TimeUnit.MILLISECONDS.toMinutes(penalty),
                TimeUnit.MILLISECONDS.toSeconds(penalty) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(penalty)),
                penalty % 1000);

        return result;
    }

    public final void initTableModel() {
        String columns[] = {"Position", "Team Name", "Time"};
        this.myTableModel = new DefaultTableModel(null, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

    public final void initTableColumnWidth() {
        this.teamResultTable.getColumnModel().getColumn(0).setMinWidth(0);
        this.teamResultTable.getColumnModel().getColumn(1).setMinWidth(0);
        this.teamResultTable.getColumnModel().getColumn(2).setMinWidth(0);

        this.teamResultTable.getColumnModel().getColumn(0).setPreferredWidth(5);
        this.teamResultTable.getColumnModel().getColumn(1).setPreferredWidth(40);
        this.teamResultTable.getColumnModel().getColumn(2).setPreferredWidth(15);
    }

    public final void syncTable() {
        myTableModel.setRowCount(0);
        listTeams();
        String kelas = classComboBox.getSelectedItem().toString();

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
            Statement statement = connection.createStatement();

            for (TeamResult tr : teamResults) {
                Integer prevParticipant = -1;
                ResultSet rs = statement.executeQuery("SELECT * FROM results NATURAL JOIN participant NATURAL JOIN team WHERE eventID = " + eventID + " ORDER BY partID ASC, class ASC;");
                while (rs.next()) {
                    if (tr.getTeamInfo().getTeamID() == rs.getInt("teamID")) {
                        if (kelas.equals("F")) {
                            if (rs.getString("class").equals("F")) {
                                tr.addTime(rs.getInt("time1"));
                                tr.addTime(rs.getInt("time2"));
                                tr.addTime(rs.getInt("time3"));

                                tr.addPenalty(rs.getInt("penalty1"));
                                tr.addPenalty(rs.getInt("penalty2"));
                                tr.addPenalty(rs.getInt("penalty3"));
                            }
                        } else {
                            if (!rs.getString("class").equals("F")) {
                                if (prevParticipant != rs.getInt("partID")) {
                                    prevParticipant = rs.getInt("partID");
                                    tr.addTime(rs.getInt("time1"));
                                    tr.addTime(rs.getInt("time2"));
                                    tr.addTime(rs.getInt("time3"));

                                    tr.addPenalty(rs.getInt("penalty1"));
                                    tr.addPenalty(rs.getInt("penalty2"));
                                    tr.addPenalty(rs.getInt("penalty3"));
                                }
                            }
                        }
                    }
                }
            }

            Collections.sort(teamResults);
            Integer position = 1;
            for (TeamResult tr : teamResults) {
                String newRow[] = new String[3];

                newRow[0] = "" + position++;
                newRow[1] = tr.getTeamInfo().getName();
                String time = getTimeFromMillisecond(tr.getTime(), tr.getPenalty());
                newRow[2] = time;

                myTableModel.addRow(newRow);
            }
        } catch (SQLException e) {
            //Logger.getLogger(TeamResultViewer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(e.toString());
        }
    }

    public final void listTeams() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
            Statement statement = connection.createStatement();

            this.teamResults = new ArrayList<>();

            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT teamID, name FROM results NATURAL JOIN participant NATURAL JOIN team WHERE eventID = " + eventID + ";");
            while (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                teamResults.add(new TeamResult(id, name));
            }
        } catch (SQLException e) {
            //Logger.getLogger(TeamResultViewer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(e.toString());
        }
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * form.
     * WARNING:
     * Do
     * NOT
     * modify
     * this
     * code.
     * The
     * content
     * of
     * this
     * method
     * is
     * always
     * regenerated
     * by
     * the
     * Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        classSelectorPanel = new javax.swing.JPanel();
        classLabel = new javax.swing.JLabel();
        classComboBox = new javax.swing.JComboBox();
        tablePanel = new javax.swing.JPanel();
        scrollPane = new javax.swing.JScrollPane();
        teamResultTable = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        classLabel.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        classLabel.setText("Class");
        classSelectorPanel.add(classLabel);

        classComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        classComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A", "F" }));
        classComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classComboBoxActionPerformed(evt);
            }
        });
        classSelectorPanel.add(classComboBox);

        add(classSelectorPanel, java.awt.BorderLayout.NORTH);

        tablePanel.setLayout(new java.awt.GridLayout(1, 1));

        teamResultTable.setModel(myTableModel);
        teamResultTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        teamResultTable.getTableHeader().setResizingAllowed(false);
        teamResultTable.getTableHeader().setReorderingAllowed(false);
        scrollPane.setViewportView(teamResultTable);

        tablePanel.add(scrollPane);

        add(tablePanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void classComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classComboBoxActionPerformed
        syncTable();
    }//GEN-LAST:event_classComboBoxActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox classComboBox;
    private javax.swing.JLabel classLabel;
    private javax.swing.JPanel classSelectorPanel;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JTable teamResultTable;
    // End of variables declaration//GEN-END:variables
}
