package ProgramEngine;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/**
 * @author
 * Jason
 */
public class EventManager extends javax.swing.JPanel {

    private String mysqlUsername = "root";
    private String mysqlPassword = "admin";
    private DefaultTableModel tableModel;

    /**
     * Creates
     * new
     * form
     * EventManager
     */
    public EventManager() {
        initTableModel();
        initComponents();
        this.showData();
    }

    private void initTableModel() {
        String columns[] = {"Event No.", "Event Name", "Location"};
        this.tableModel = new DefaultTableModel(null, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

    /**
     * WARNING:
     * THIS
     * IS
     * STILL
     * IN
     * TESTING
     * PHASE!
     * Show
     * data
     * from
     * database
     * to
     * table.
     */
    private void showData() {
        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
            PreparedStatement ps = con.prepareStatement("SELECT * FROM event ORDER BY eventID;");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String[] data = new String[3];
                data[0] = Integer.toString(rs.getInt("eventID"));
                data[1] = rs.getString("name");
                data[2] = rs.getString("location");

                this.tableModel.addRow(data);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EventManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void refresh() {
        this.tableModel = null;
        this.initTableModel();
        this.showData();
        this.eventTable.setModel(tableModel);
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * form.
     * WARNING:
     * Do
     * NOT
     * modify
     * this
     * code.
     * The
     * content
     * of
     * this
     * method
     * is
     * always
     * regenerated
     * by
     * the
     * Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        eventTable = new javax.swing.JTable();
        editEventButton = new javax.swing.JButton();
        addEventButton = new javax.swing.JButton();
        titleLabel = new javax.swing.JLabel();
        refreshButton = new javax.swing.JButton();

        eventTable.setModel(tableModel);
        eventTable.setName("Events"); // NOI18N
        eventTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        eventTable.getTableHeader().setResizingAllowed(false);
        eventTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(eventTable);

        editEventButton.setText("Edit Event");
        editEventButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editEventButtonActionPerformed(evt);
            }
        });

        addEventButton.setText("Add Event");
        addEventButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addEventButtonActionPerformed(evt);
            }
        });

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        titleLabel.setText("Event Manager Dashboard");

        refreshButton.setText("Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(refreshButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 379, Short.MAX_VALUE)
                        .addComponent(addEventButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editEventButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(titleLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editEventButton)
                    .addComponent(addEventButton)
                    .addComponent(refreshButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Edit
     * event
     * button
     * action
     * listener.
     * Action
     * performed
     * on
     * this
     * button
     * should
     * pop
     * up
     * a
     * dialog/window
     * to
     * select
     * which
     * event
     * to
     * edit.
     * This
     * function
     * could
     * possibly
     * be
     * dropped
     *
     * @param
     * evt
     */
    private void editEventButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editEventButtonActionPerformed
        try {
            JFrame adderFrame = new JFrame("Edit Event");
            adderFrame.setSize(320, 180);

            JPanel adderPanel = new EventEditor(this,
                    adderFrame,
                    Integer.parseInt(this.tableModel.getValueAt(
                    this.eventTable.getSelectedRow(), 0).toString()));

            adderFrame.add(adderPanel); //Adds event adder to the window

            adderFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            adderFrame.setResizable(false);

            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            adderFrame.setLocation((dim.width - 320) / 2, (dim.height - 180) / 3);

            adderFrame.setVisible(true);
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(this, "Please select an event to edit!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_editEventButtonActionPerformed

    /**
     * Add
     * event
     * button
     * action
     * listener.
     * Action
     * performed
     * on
     * this
     * button
     * will
     * open
     * an
     * event
     * adder
     * window.
     *
     * @param
     * evt
     */
    private void addEventButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addEventButtonActionPerformed
        JFrame adderFrame = new JFrame("Add Event");
        adderFrame.setSize(320, 180);

        JPanel adderPanel = new EventAdder(this, adderFrame);
        adderFrame.add(adderPanel); //Adds event adder to the window

        adderFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        adderFrame.setResizable(false);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        adderFrame.setLocation((dim.width - 320) / 2, (dim.height - 180) / 3);

        adderFrame.setVisible(true);
    }//GEN-LAST:event_addEventButtonActionPerformed

    /**
     * Refresh
     * button
     * action
     * listener.
     *
     * @param
     * evt
     */
    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        this.refresh();
    }//GEN-LAST:event_refreshButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addEventButton;
    private javax.swing.JButton editEventButton;
    private javax.swing.JTable eventTable;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton refreshButton;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
