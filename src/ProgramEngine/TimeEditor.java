/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramEngine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author
 * Jason
 */
public class TimeEditor extends javax.swing.JPanel {

    private String mysqlUsername = "root";
    private String mysqlPassword = "";
    private MainProgram parent;
    private Integer eventID;
    private Integer noStart;

    /**
     * Creates
     * new
     * form
     * TimeEditor
     */
    public TimeEditor(MainProgram parent, Integer eventID, Integer noStart) {
        this.parent = parent;
        this.eventID = eventID;
        this.noStart = noStart;
        initComponents();
        initComboBoxes();
    }

    private void initComboBoxes() {
        for (int i = 0; i <= 59; i++) {
            String minute = String.format("%02d", i);
            heat1MinuteComboBox.addItem(minute);
            heat1MinutePenaltyComboBox.addItem(minute);
            heat2MinuteComboBox.addItem(minute);
            heat2MinutePenaltyComboBox.addItem(minute);
            heat3MinuteComboBox.addItem(minute);
            heat3MinutePenaltyComboBox.addItem(minute);

            String second = String.format("%02d", i);
            heat1SecondComboBox.addItem(second);
            heat1SecondPenaltyComboBox.addItem(second);
            heat2SecondComboBox.addItem(second);
            heat2SecondPenaltyComboBox.addItem(second);
            heat3SecondComboBox.addItem(second);
            heat3SecondPenaltyComboBox.addItem(second);
        }

        for (int i = 0; i <= 999; i++) {
            String millisecond = String.format("%03d", i);
            heat1MillisecondComboBox.addItem(millisecond);
            heat1MillisecondPenaltyComboBox.addItem(millisecond);
            heat2MillisecondComboBox.addItem(millisecond);
            heat2MillisecondPenaltyComboBox.addItem(millisecond);
            heat3MillisecondComboBox.addItem(millisecond);
            heat3MillisecondPenaltyComboBox.addItem(millisecond);
        }

        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
            Statement statement = connection.createStatement();

            Integer heat1 = 0, heat2 = 0, heat3 = 0;
            Integer penalty1 = 0, penalty2 = 0, penalty3 = 0;

            ResultSet resultSet = statement.executeQuery("SELECT * FROM results WHERE eventID = " + eventID + " AND startNo = " + noStart + ";");
            while (resultSet.next()) {
                heat1 = Integer.parseInt(resultSet.getString("time1"));
                heat2 = Integer.parseInt(resultSet.getString("time2"));
                heat3 = Integer.parseInt(resultSet.getString("time3"));

                penalty1 = Integer.parseInt(resultSet.getString("penalty1"));
                penalty2 = Integer.parseInt(resultSet.getString("penalty2"));
                penalty3 = Integer.parseInt(resultSet.getString("penalty3"));
            }

            heat1MinuteComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toMinutes(heat1));
            heat2MinuteComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toMinutes(heat2));
            heat3MinuteComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toMinutes(heat3));
            heat1SecondComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toSeconds(heat1) - (int) TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(heat1)));
            heat2SecondComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toSeconds(heat2) - (int) TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(heat2)));
            heat3SecondComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toSeconds(heat3) - (int) TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(heat3)));
            heat1MillisecondComboBox.setSelectedIndex(heat1 % 1000);
            heat2MillisecondComboBox.setSelectedIndex(heat2 % 1000);
            heat3MillisecondComboBox.setSelectedIndex(heat3 % 1000);

            heat1MinutePenaltyComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toMinutes(penalty1));
            heat2MinutePenaltyComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toMinutes(penalty2));
            heat3MinutePenaltyComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toMinutes(penalty3));
            heat1SecondPenaltyComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toSeconds(penalty1) - (int) TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(penalty1)));
            heat2SecondPenaltyComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toSeconds(penalty2) - (int) TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(penalty2)));
            heat3SecondPenaltyComboBox.setSelectedIndex((int) TimeUnit.MILLISECONDS.toSeconds(penalty3) - (int) TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(penalty3)));
            heat1MillisecondPenaltyComboBox.setSelectedIndex(penalty1 % 1000);
            heat2MillisecondPenaltyComboBox.setSelectedIndex(penalty2 % 1000);
            heat3MillisecondPenaltyComboBox.setSelectedIndex(penalty3 % 1000);
        } catch (SQLException e) {
            //Logger.getLogger(TimeEditor.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(e.toString());
        }
    }

    private Integer timeToMillisecond(Integer minute, Integer second, Integer millisecond) {
        Integer result = 0;

        result += minute * 60000;
        result += second * 1000;
        result += millisecond;

        return result;
    }

    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * form.
     * WARNING:
     * Do
     * NOT
     * modify
     * this
     * code.
     * The
     * content
     * of
     * this
     * method
     * is
     * always
     * regenerated
     * by
     * the
     * Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        heat1Label = new javax.swing.JLabel();
        heat2Label = new javax.swing.JLabel();
        heat3Label = new javax.swing.JLabel();
        saveBtn = new javax.swing.JButton();
        heat1MinuteComboBox = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        heat1SecondComboBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        heat1MillisecondPenaltyComboBox = new javax.swing.JComboBox();
        heat2MinuteComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        heat2SecondComboBox = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        heat2MillisecondComboBox = new javax.swing.JComboBox();
        heat3MinuteComboBox = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        heat3SecondComboBox = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        heat3MillisecondComboBox = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        heat1MinutePenaltyComboBox = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        heat1SecondPenaltyComboBox = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        heat1MillisecondComboBox = new javax.swing.JComboBox();
        heat2MinutePenaltyComboBox = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        heat2SecondPenaltyComboBox = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        heat2MillisecondPenaltyComboBox = new javax.swing.JComboBox();
        heat3MinutePenaltyComboBox = new javax.swing.JComboBox();
        heat3SecondPenaltyComboBox = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        heat3MillisecondPenaltyComboBox = new javax.swing.JComboBox();

        heat1Label.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat1Label.setText("Heat 1");

        heat2Label.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat2Label.setText("Heat 2");

        heat3Label.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat3Label.setText("Heat 3");

        saveBtn.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        saveBtn.setText("Save");
        saveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveBtnMouseClicked(evt);
            }
        });

        heat1MinuteComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat1MinuteComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText(":");

        heat1SecondComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat1SecondComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel2.setText(":");

        heat1MillisecondPenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat1MillisecondPenaltyComboBox.setPreferredSize(new java.awt.Dimension(75, 26));

        heat2MinuteComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat2MinuteComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel3.setText(":");

        heat2SecondComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat2SecondComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel4.setText(":");

        heat2MillisecondComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat2MillisecondComboBox.setPreferredSize(new java.awt.Dimension(75, 26));

        heat3MinuteComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat3MinuteComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel5.setText(":");

        heat3SecondComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat3SecondComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel6.setText(":");

        heat3MillisecondComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat3MillisecondComboBox.setPreferredSize(new java.awt.Dimension(75, 26));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel7.setText("Penalty");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel8.setText("Penalty");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel9.setText("Penalty");

        heat1MinutePenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat1MinutePenaltyComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel10.setText(":");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel11.setText(":");

        heat1SecondPenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat1SecondPenaltyComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel12.setText(":");

        heat1MillisecondComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat1MillisecondComboBox.setPreferredSize(new java.awt.Dimension(75, 26));

        heat2MinutePenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat2MinutePenaltyComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel13.setText(":");

        heat2SecondPenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat2SecondPenaltyComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel14.setText(":");

        heat2MillisecondPenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat2MillisecondPenaltyComboBox.setPreferredSize(new java.awt.Dimension(75, 26));

        heat3MinutePenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat3MinutePenaltyComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        heat3SecondPenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat3SecondPenaltyComboBox.setPreferredSize(new java.awt.Dimension(50, 26));

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel16.setText(":");

        heat3MillisecondPenaltyComboBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        heat3MillisecondPenaltyComboBox.setPreferredSize(new java.awt.Dimension(75, 26));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(saveBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(heat3Label)
                                .addGap(18, 18, 18)
                                .addComponent(heat3MinuteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat3SecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat3MillisecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel9))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(heat2Label)
                                .addGap(18, 18, 18)
                                .addComponent(heat2MinuteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat2SecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat2MillisecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(heat1Label)
                                .addGap(18, 18, 18)
                                .addComponent(heat1MinuteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat1SecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat1MillisecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel7)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(heat1MinutePenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat1SecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat1MillisecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(heat2MinutePenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat2SecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat2MillisecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(heat3MinutePenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat3SecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(heat3MillisecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(heat1Label)
                    .addComponent(heat1MinuteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(heat1SecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(heat1MillisecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(heat1MinutePenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(heat1SecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(heat1MillisecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(heat2MinuteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(heat2Label)
                    .addComponent(jLabel3)
                    .addComponent(heat2SecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(heat2MillisecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(heat2MinutePenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(heat2SecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(heat2MillisecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(heat3Label)
                    .addComponent(heat3MinuteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(heat3SecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(heat3MillisecondComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(heat3MinutePenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(heat3SecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(heat3MillisecondPenaltyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(saveBtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void saveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveBtnMouseClicked
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/djarum_slalom", mysqlUsername, mysqlPassword);
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE results SET time1 = ?, time2 = ?, time3 = ?, penalty1 = ?, penalty2 = ?, penalty3 = ? WHERE eventID = " + eventID + " AND startNo = " + noStart + ";");

            Integer heat1 = timeToMillisecond(heat1MinuteComboBox.getSelectedIndex(), heat1SecondComboBox.getSelectedIndex(), heat1MillisecondComboBox.getSelectedIndex());
            Integer heat2 = timeToMillisecond(heat2MinuteComboBox.getSelectedIndex(), heat2SecondComboBox.getSelectedIndex(), heat2MillisecondComboBox.getSelectedIndex());
            Integer heat3 = timeToMillisecond(heat3MinuteComboBox.getSelectedIndex(), heat3SecondComboBox.getSelectedIndex(), heat3MillisecondComboBox.getSelectedIndex());
            Integer penalty1 = timeToMillisecond(heat1MinutePenaltyComboBox.getSelectedIndex(), heat1SecondPenaltyComboBox.getSelectedIndex(), heat1MillisecondPenaltyComboBox.getSelectedIndex());
            Integer penalty2 = timeToMillisecond(heat2MinutePenaltyComboBox.getSelectedIndex(), heat2SecondPenaltyComboBox.getSelectedIndex(), heat2MillisecondPenaltyComboBox.getSelectedIndex());
            Integer penalty3 = timeToMillisecond(heat3MinutePenaltyComboBox.getSelectedIndex(), heat3SecondPenaltyComboBox.getSelectedIndex(), heat3MillisecondPenaltyComboBox.getSelectedIndex());

            preparedStatement.setInt(1, heat1);
            preparedStatement.setInt(2, heat2);
            preparedStatement.setInt(3, heat3);
            preparedStatement.setInt(4, penalty1);
            preparedStatement.setInt(5, penalty2);
            preparedStatement.setInt(6, penalty3);
            preparedStatement.executeUpdate();

            parent.syncTable();
            JFrame windowAncestor = (JFrame)SwingUtilities.getWindowAncestor(this);
            windowAncestor.dispose();
        } catch (SQLException e) {
            //Logger.getLogger(TimeEditor.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(e.toString());
        }
    }//GEN-LAST:event_saveBtnMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel heat1Label;
    private javax.swing.JComboBox heat1MillisecondComboBox;
    private javax.swing.JComboBox heat1MillisecondPenaltyComboBox;
    private javax.swing.JComboBox heat1MinuteComboBox;
    private javax.swing.JComboBox heat1MinutePenaltyComboBox;
    private javax.swing.JComboBox heat1SecondComboBox;
    private javax.swing.JComboBox heat1SecondPenaltyComboBox;
    private javax.swing.JLabel heat2Label;
    private javax.swing.JComboBox heat2MillisecondComboBox;
    private javax.swing.JComboBox heat2MillisecondPenaltyComboBox;
    private javax.swing.JComboBox heat2MinuteComboBox;
    private javax.swing.JComboBox heat2MinutePenaltyComboBox;
    private javax.swing.JComboBox heat2SecondComboBox;
    private javax.swing.JComboBox heat2SecondPenaltyComboBox;
    private javax.swing.JLabel heat3Label;
    private javax.swing.JComboBox heat3MillisecondComboBox;
    private javax.swing.JComboBox heat3MillisecondPenaltyComboBox;
    private javax.swing.JComboBox heat3MinuteComboBox;
    private javax.swing.JComboBox heat3MinutePenaltyComboBox;
    private javax.swing.JComboBox heat3SecondComboBox;
    private javax.swing.JComboBox heat3SecondPenaltyComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton saveBtn;
    // End of variables declaration//GEN-END:variables
}
