package ProgramEngine;

import Objects.Team;
import dataaccess.DBUtil;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Kevin
 */
public class TeamEditor extends TeamAdder {
    private Team t;
    
    public TeamEditor(JFrame container, int teamID) {
        super(container);
        t = new Team(teamID, new DBUtil().getTeamNameByID(teamID));
        
        this.titleLabel.setText("Edit a team");
        
        this.teamNameField.setText(this.t.getName());
        
        
        //Remove the super action listener on submit button
        for (java.awt.event.ActionListener l : submitButton.getActionListeners()) {
            submitButton.removeActionListener(l);
        }
        
        //Add the new action listener
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });
    }
    
    /**
     * Submit button action listener
     * @param evt 
     */
    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        if (this.teamNameField.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "The team name field should not be left empty.", "Message", JOptionPane.ERROR_MESSAGE);
        } else if (new DBUtil().updateTeam(this.t.getTeamID(), this.teamNameField.getText())) {
            JOptionPane.showMessageDialog(null, "Data update successful!", "Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            
            if (this.container != null) {
                this.container.setVisible(false);
                this.container.dispose();
            }
            
            TeamManager.refresh();
        }
    }
}
