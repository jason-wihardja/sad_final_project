package ProgramEngine;

import Objects.Event;
import dataaccess.DBUtil;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Event editor panel
 *
 * @author Kevin
 */
public class EventEditor extends EventAdder {

    private Event e;

    public EventEditor(EventManager caller, JFrame container, int eventID) {
        super(caller, container);
        this.e = new DBUtil().getEventByID(eventID);
        
        this.titleLabel.setText("Edit an Event");
        
        this.eventNameField.setText(this.e.getName());
        this.locationField.setText(this.e.getLocation());

        //Remove the super action listener on submit button
        for (java.awt.event.ActionListener l : submitButton.getActionListeners()) {
            submitButton.removeActionListener(l);
        }
        
        //Add the new action listener
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });
    }

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if (this.eventNameField.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Event name field should not be left empty.", "Message", JOptionPane.ERROR_MESSAGE);
        } else if (this.locationField.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Event location field should not be left empty.", "Message", JOptionPane.ERROR_MESSAGE);
        } else {
            if (new DBUtil().updateEvent(this.e.getEventID(), this.eventNameField.getText(), this.locationField.getText())) {
                javax.swing.JOptionPane.showMessageDialog(null, "Data update successful!", "Message", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                if (this.parent != null) {
                    this.parent.setVisible(false);
                    this.parent.dispose();
                }

                if (this.caller != null) {
                    this.caller.refresh();
                }
            } else {
                javax.swing.JOptionPane.showMessageDialog(null, "Error updating data to DB!", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
