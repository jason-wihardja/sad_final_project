package ProgramEngine;

import Objects.Team;
import dataaccess.DBUtil;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/**
 * @author Jason
 */
public class TeamManager extends javax.swing.JPanel {
    
    protected static DefaultTableModel tableModel;
    
    /**
     * Creates
     * new
     * form
     * TeamManager
     */
    public TeamManager() {
        initTableModel();
        initComponents();
        showData();
        
        teamTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION); //Can only select single row
    }
    
    private static void initTableModel() {
        String columns[] = {"Team ID", "Team Name"};
        TeamManager.tableModel = new DefaultTableModel(null, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
    
    public static void refresh() {
        TeamManager.tableModel = null;
        initTableModel();
        TeamManager.teamTable.setModel(tableModel);
        showData();
    }
    
    private static void showData() {
        ArrayList<Team> list = new DBUtil().getTeams();
        
        String[] data = new String[2];
        for (int i = 0; i < list.size(); i++) {
            data[0] = Integer.toString(list.get(i).getTeamID());
            data[1] = list.get(i).getName();
            
            TeamManager.tableModel.addRow(data);
        }
    }
    
    /**
     * This
     * method
     * is
     * called
     * from
     * within
     * the
     * constructor
     * to
     * initialize
     * the
     * form.
     * WARNING:
     * Do
     * NOT
     * modify
     * this
     * code.
     * The
     * content
     * of
     * this
     * method
     * is
     * always
     * regenerated
     * by
     * the
     * Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        teamTable = new javax.swing.JTable();
        editTeamButton = new javax.swing.JButton();
        addTeamButton = new javax.swing.JButton();
        refreshButton = new javax.swing.JButton();

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        titleLabel.setText("Team Manager Dashboard");

        teamTable.setModel(tableModel);
        teamTable.getTableHeader().setResizingAllowed(false);
        teamTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(teamTable);

        editTeamButton.setText("Edit Team");
        editTeamButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editTeamButtonActionPerformed(evt);
            }
        });

        addTeamButton.setText("Add Team");
        addTeamButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTeamButtonActionPerformed(evt);
            }
        });

        refreshButton.setText("Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(titleLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(refreshButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addTeamButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editTeamButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editTeamButton)
                    .addComponent(addTeamButton)
                    .addComponent(refreshButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addTeamButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addTeamButtonActionPerformed
        JFrame adderFrame = new JFrame("Add Team");
        adderFrame.setSize(320, 180);
        
        JPanel adderPanel = new TeamAdder(adderFrame);
        adderFrame.add(adderPanel); //Adds event adder to the window
        
        adderFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        adderFrame.setResizable(false);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        adderFrame.setLocation((dim.width - 320) / 2, (dim.height - 180) / 3);

        adderFrame.setVisible(true);
    }//GEN-LAST:event_addTeamButtonActionPerformed

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        TeamManager.refresh();
    }//GEN-LAST:event_refreshButtonActionPerformed

    private void editTeamButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editTeamButtonActionPerformed
        try {
            JFrame adderFrame = new JFrame("Edit Team");
            adderFrame.setSize(320, 180);
            
            JPanel adderPanel = new TeamEditor(adderFrame,
                    Integer.parseInt(TeamManager.tableModel.getValueAt(TeamManager.teamTable.getSelectedRow(), 0).toString()));
            adderFrame.add(adderPanel); //Adds event adder to the window
            
            adderFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            adderFrame.setResizable(false);
            
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            adderFrame.setLocation((dim.width - 320) / 2, (dim.height - 180) / 3);
            
            adderFrame.setVisible(true);
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(this, "Please select a team to edit!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_editTeamButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addTeamButton;
    private javax.swing.JButton editTeamButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton refreshButton;
    private static javax.swing.JTable teamTable;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
