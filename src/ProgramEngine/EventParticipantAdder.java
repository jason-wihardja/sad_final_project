/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramEngine;

import Objects.Participant;
import dataaccess.DBUtil;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Kevin
 */
public class EventParticipantAdder extends javax.swing.JPanel {

    private JFrame container;
    private MainProgram mainProgram;
    private int eventID;
    private ArrayList<Participant> partList;

    /**
     * Creates new form EventParticipantAdder
     */
    public EventParticipantAdder() {
        this.partList = new DBUtil().getParticipants();
        this.container = null;
        initComponents();
        initParticipantBox();
    }

    public EventParticipantAdder(MainProgram m, JFrame container, int eventID) {
        this.mainProgram = m;
        this.container = container;
        this.eventID = eventID;
        this.partList = new DBUtil().getParticipants();
        initComponents();
        initParticipantBox();
    }

    private void initParticipantBox() {
        if (this.partList == null) {
            JOptionPane.showMessageDialog(null, "Team list is null!", "Error!", JOptionPane.ERROR_MESSAGE);
            if (this.container != null) {
                this.container.setVisible(false);
                this.container.dispose();
            }
        } else {
            String[] data = new String[partList.size()];

            for (int i = 0; i < partList.size(); i++) {
                data[i] = partList.get(i).getPartID() + " - " + partList.get(i).getfName() + " " + partList.get(i).getlName();
            }

            this.participantBox.setModel(new DefaultComboBoxModel(data));
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        participantLabel = new javax.swing.JLabel();
        startNoLabel = new javax.swing.JLabel();
        classLabel = new javax.swing.JLabel();
        carLabel = new javax.swing.JLabel();
        participantBox = new javax.swing.JComboBox();
        startNoField = new javax.swing.JTextField();
        classBox = new javax.swing.JComboBox();
        carField = new javax.swing.JTextField();
        submitButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        titleLabel.setText("Add a Participant to Event");

        participantLabel.setText("Participant");

        startNoLabel.setText("Start/car number");

        classLabel.setText("Class");

        carLabel.setText("Car name/model");

        participantBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        classBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A1", "A2", "A3", "AS", "B", "C", "D", "F", "F GRB" }));

        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(resetButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(submitButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(titleLabel)
                            .addComponent(participantLabel)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(carLabel)
                                .addGap(22, 22, 22)
                                .addComponent(carField, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(startNoLabel)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(participantBox, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(startNoField, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(classBox, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(classLabel))
                        .addGap(0, 31, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(participantLabel)
                    .addComponent(participantBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(startNoLabel)
                            .addComponent(startNoField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(classLabel)
                            .addComponent(classBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(carLabel)
                            .addComponent(carField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(53, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(submitButton)
                            .addComponent(resetButton))
                        .addContainerGap())))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        int partID = this.partList.get(this.participantBox.getSelectedIndex()).getPartID();
        int startNo;

        String cls = this.classBox.getSelectedItem().toString();
        String car = this.carField.getText();

        if (this.startNoField.getText().isEmpty() || this.startNoField.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Start number field must not be left empty", "Error!", JOptionPane.ERROR_MESSAGE);
        } else if (!this.startNoField.getText().matches("\\d+")) {
            JOptionPane.showMessageDialog(null, "Start Number field value is not a number", "Error!", JOptionPane.ERROR_MESSAGE);
        } else if (this.carField.getText().isEmpty() || this.carField.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Car name field value must not be left empty", "Error!", JOptionPane.ERROR_MESSAGE);
        } else {
            startNo = Integer.parseInt(this.startNoField.getText());

            //JOptionPane.showMessageDialog(null, partID + "\n" + startNo + "\n" + cls + "\n" + car);

            if (new DBUtil().addParticipantToResult(eventID, partID, startNo, cls, car)) {
                JOptionPane.showMessageDialog(null, "Data input successful", "Message", JOptionPane.INFORMATION_MESSAGE);

                RacerManager.refresh();
                mainProgram.syncTable();
                if (this.container != null) {
                    RacerManager.refresh();
                    this.container.setVisible(false);
                    this.container.dispose();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Data input failed", "Error!", JOptionPane.ERROR_MESSAGE);
            }

        }


    }//GEN-LAST:event_submitButtonActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        this.participantBox.setSelectedIndex(0);
        this.startNoField.setText("");
        this.classBox.setSelectedIndex(0);
        this.carField.setText("");
    }//GEN-LAST:event_resetButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField carField;
    private javax.swing.JLabel carLabel;
    private javax.swing.JComboBox classBox;
    private javax.swing.JLabel classLabel;
    private javax.swing.JComboBox participantBox;
    private javax.swing.JLabel participantLabel;
    private javax.swing.JButton resetButton;
    private javax.swing.JTextField startNoField;
    private javax.swing.JLabel startNoLabel;
    private javax.swing.JButton submitButton;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
