package dataaccess;

import Objects.Event;
import Objects.Participant;
import Objects.Team;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Database utility
 *
 * @author Kevin
 */
public class DBUtil {

    private String db_user;
    private String db_password;
    private String db_url;
    private String driver;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    /**
     * Blank constructor
     */
    public DBUtil() {
        this.db_user = "root";
        this.db_password = "admin"; //MySQL Server password
        this.db_url = "jdbc:mysql://localhost/djarum_slalom";
        this.driver = "com.mysql.jdbc.Driver";

        this.con = null;
        this.ps = null;
        this.rs = null;
    }

    /**
     * Establish DB connection.
     *
     * @return true if connection successful, false otherwise.
     */
    public boolean connect() {
        if (con == null) {
            try {
                Class.forName(this.driver);
                con = DriverManager.getConnection(db_url, db_user, db_password);

                return con != null;
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return true;
        }
    }

    //Methods for Event --------------------------------------------------------
    /**
     * Add an event to database
     *
     * @param name event name
     * @param location event location
     * @return true if insertion successful, false otherwise
     */
    public boolean addEvent(String name, String location) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM event WHERE name=?");
                this.ps.setString(1, name);

                if (this.ps.executeQuery().next()) {
                    return false;
                }

                this.ps = this.con.prepareStatement("INSERT "
                        + "INTO event (name, location) VALUES (?,?)");
                this.ps.setString(1, name);
                this.ps.setString(2, location);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get event data from database
     *
     * @return an ArrayList of Event
     */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> list = new ArrayList<>();
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM event");

                this.rs = this.ps.executeQuery();

                while (this.rs.next()) {
                    list.add(new Event(this.rs.getInt("eventID"), this.rs.getString("name"), this.rs.getString("location")));
                }

                this.rs.close();
                this.ps.close();
                this.con.close();

                return list;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Update event data function.
     *
     * @param eventID event ID
     * @param name event name
     * @param location event location
     * @return true if update success, false otherwise
     */
    public boolean updateEvent(int eventID, String name, String location) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("UPDATE event "
                        + "SET name=?, location=? "
                        + "WHERE eventID=?");
                this.ps.setString(1, name);
                this.ps.setString(2, location);
                this.ps.setInt(3, eventID);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Delete an event from database.
     *
     * @param eventID event ID to be deleted
     * @return true if deletion successful, false otherwise
     */
    public boolean dropEvent(int eventID) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("DELETE FROM event "
                        + "WHERE eventID=?");
                this.ps.setInt(1, eventID);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }


    }

    /**
     * Get event data from database by event ID
     *
     * @param eventID event ID
     * @return Event object
     */
    public Event getEventByID(int eventID) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM event "
                        + "WHERE eventID=?");
                this.ps.setInt(1, eventID);

                this.rs = this.ps.executeQuery();

                if (rs.next()) {
                    return new Event(rs.getInt("eventID"), rs.getString("name"), rs.getString("location"));
                } else {
                    return null;
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            return null;
        }
    }

    //Methods for Participant (Racer) ------------------------------------------
    /**
     * Add a participant to database.
     *
     * @param fName
     * @param lName
     * @param category
     * @param teamID
     * @param origin
     * @return
     */
    public boolean addParticipant(String fName, String lName, String category,
            int teamID, String origin) {

        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM participant "
                        + "WHERE fName=? AND lName=?");

                this.ps.setString(1, fName);
                this.ps.setString(2, lName);

                if (this.ps.executeQuery().next()) {
                    return false;
                }

                this.ps = this.con.prepareStatement("INSERT INTO participant "
                        + "(fName, lName, category, teamID, origin) VALUES "
                        + "(?,?,?,?,?)");

                this.ps.setString(1, fName);
                this.ps.setString(2, lName);
                this.ps.setString(3, category);
                this.ps.setInt(4, teamID);
                this.ps.setString(5, origin);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get participant data from database
     *
     * @return
     */
    public ArrayList<Participant> getParticipants() {
        ArrayList<Participant> list = new ArrayList<>();
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM participant");

                this.rs = this.ps.executeQuery();

                while (rs.next()) {
                    int partID = rs.getInt("partID");
                    String fName = rs.getString("fName");
                    String lName = rs.getString("lName");
                    String category = rs.getString("category");
                    int teamID = rs.getInt("teamID");
                    String origin = rs.getString("origin");

                    list.add(new Participant(partID, fName, lName, category, teamID, origin));
                }

                this.rs.close();
                this.ps.close();
                this.con.close();

                return list;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }

        }

        return null;
    }

    /**
     * Update participant data.
     *
     * @param partID
     * @param fName
     * @param lName
     * @param category
     * @param teamID
     * @param origin
     * @return
     */
    public boolean updateParticipant(int partID, String fName, String lName,
            String category, int teamID, String origin) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("UPDATE participant "
                        + "SET fName=?, lName=?, category=?, teamID=?, origin=? "
                        + "WHERE partID=?");

                this.ps.setString(1, fName);
                this.ps.setString(2, lName);
                this.ps.setString(3, category);
                this.ps.setInt(4, teamID);
                this.ps.setString(5, origin);
                this.ps.setInt(6, partID);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Delete participant data from database.
     *
     * @param partID
     * @return
     */
    public boolean dropParticipant(int partID) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("DELETE FROM participant "
                        + "WHERE partID=?");
                this.ps.setInt(1, partID);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get participant object from database by participant ID.
     *
     * @param partID participant ID
     * @return Participant object
     */
    public Participant getParticipantByID(int partID) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM participant "
                        + "WHERE partID=?");
                this.ps.setInt(1, partID);

                this.rs = this.ps.executeQuery();

                if (rs.next()) {
                    return new Participant(
                            this.rs.getInt("partID"),
                            this.rs.getString("fName"),
                            this.rs.getString("lName"),
                            this.rs.getString("category"),
                            this.rs.getInt("teamID"),
                            this.rs.getString("origin"));
                } else {
                    return null;
                }

            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            return null;
        }
    }

    //Methods for Team ---------------------------------------------------------
    /**
     * Add a team to database
     *
     * @param name team name
     * @return true if insertion successful, false otherwise.
     */
    public boolean addTeam(String name) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM team WHERE name=?");
                this.ps.setString(1, name);

                if (this.ps.executeQuery().next()) {
                    return false;
                }

                this.ps = this.con.prepareStatement("INSERT INTO team (name) "
                        + "VALUES (?)");
                this.ps.setString(1, name);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get team data from database
     *
     * @return ArrayList of team
     */
    public ArrayList<Team> getTeams() {
        ArrayList<Team> list = new ArrayList<>();
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT * FROM team");

                this.rs = this.ps.executeQuery();

                while (rs.next()) {
                    int teamID = rs.getInt("teamID");
                    String name = rs.getString("name");

                    list.add(new Team(teamID, name));
                }

                this.rs.close();
                this.ps.close();
                this.con.close();

                return list;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

        return null;
    }

    /**
     * Update a team.
     *
     * @param teamID team ID of which the name would be updated
     * @param name updated name
     * @return true if update successful, false otherwise.
     */
    public boolean updateTeam(int teamID, String name) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("UPDATE team "
                        + "SET name=? WHERE teamID=?");
                this.ps.setString(1, name);
                this.ps.setInt(2, teamID);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Delete a team from database
     *
     * @param teamID team ID to be deleted
     * @return true if deletion successful, false otherwise.
     */
    public boolean dropTeam(int teamID) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("DELETE FROM team "
                        + "WHERE teamID=?");
                this.ps.setInt(1, teamID);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }

    //Other methods ------------------------------------------------------------
    /**
     * Get team name by ID
     *
     * @param id team id
     * @return string of team name
     */
    public String getTeamNameByID(int id) {
        if (this.connect()) {
            try {
                String name = null;
                this.ps = this.con.prepareStatement("SELECT * FROM team WHERE teamID=?");
                this.ps.setInt(1, id);

                this.rs = this.ps.executeQuery();

                if (rs.next()) {
                    name = rs.getString("name");
                }

                return name;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Get everyone that participates in a certain event.
     *
     * @param id Event ID
     * @return ArrayList of Participant
     */
    public ArrayList<Participant> getParticipantsPerEvent(int id) {
        ArrayList<Participant> list = new ArrayList<>();
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("SELECT DISTINCT partID, fName, "
                        + "lName, category, teamID, origin FROM participant p "
                        + "NATURAL JOIN results r WHERE p.partID=r.partID "
                        + "AND eventID=?");
                this.ps.setInt(1, id);

                this.rs = this.ps.executeQuery();

                while (rs.next()) {
                    int partID = rs.getInt("partID");
                    String fName = rs.getString("fName");
                    String lName = rs.getString("lName");
                    String category = rs.getString("category");
                    int teamID = rs.getInt("teamID");
                    String origin = rs.getString("origin");

                    list.add(new Participant(partID, fName, lName, category, teamID, origin));
                }

                this.rs.close();
                this.ps.close();
                this.con.close();

                return list;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }

        }

        return null;
    }

    public boolean addParticipantToResult(int eventID, int partID,
            int startNo, String cls, String car) {

        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("INSERT INTO results "
                        + "(eventID, partID, startNo, class, car, time1, time2, "
                        + "time3, penalty1, penalty2, penalty3) "
                        + "VALUES (?,?,?,?,?,0,0,0,0,0,0)");
                this.ps.setInt(1, eventID);
                this.ps.setInt(2, partID);
                this.ps.setInt(3, startNo);
                this.ps.setString(4, cls);
                this.ps.setString(5, car);

                this.ps.executeUpdate();

                this.ps.close();
                this.con.close();

                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }
    
    public boolean dropRacerFromResult(int eventID, int partID) {
        if (this.connect()) {
            try {
                this.ps = this.con.prepareStatement("DELETE FROM Results "
                        + "WHERE eventID=? AND partID=?");
                this.ps.setInt(1, eventID);
                this.ps.setInt(2, partID);
                
                this.ps.executeUpdate();
                
                this.ps.close();
                this.con.close();
                
                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            return false;
        }
    }
}
